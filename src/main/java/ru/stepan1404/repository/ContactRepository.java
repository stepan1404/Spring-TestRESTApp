package ru.stepan1404.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.stepan1404.dto.ContactWidthdrawing;
import ru.stepan1404.entity.Contact;

import java.util.List;

public interface ContactRepository extends JpaRepository<Contact, Integer> {

    @Query("SELECT new ru.stepan1404.dto.ContactWidthdrawing(c.name, sum(d.price)) FROM Contact c left outer join Delivery d on c = d.contact group by c.name")
    public List<ContactWidthdrawing> getContactWithdrawing();

}
