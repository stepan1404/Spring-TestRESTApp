package ru.stepan1404.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.stepan1404.entity.Delivery;

public interface DeliveryRepository extends JpaRepository<Delivery, Integer> {

}
