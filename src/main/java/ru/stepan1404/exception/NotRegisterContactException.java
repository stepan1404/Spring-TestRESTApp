package ru.stepan1404.exception;

public class NotRegisterContactException extends Exception {

    public NotRegisterContactException(String message){
        super(message);
    }
}
