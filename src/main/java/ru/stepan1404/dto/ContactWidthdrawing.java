package ru.stepan1404.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContactWidthdrawing {

    private String name;
    private Long totalPrice;

    public ContactWidthdrawing(String name, Long totalPrice){
        this.name = name;
        if(totalPrice != null)
            this.totalPrice = totalPrice;
        else
            this.totalPrice = 0L;
    }
}
