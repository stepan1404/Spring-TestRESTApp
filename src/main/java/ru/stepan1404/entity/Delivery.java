package ru.stepan1404.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "delivery")
@Getter
@Setter
public class Delivery {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "createDate")
    private LocalDate createDate;

    @Column(name = "productName")
    private String productName;

    @Column(name = "price")
    private Integer price;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contact_id", nullable = false)
    @JsonIgnore
    private Contact contact;

    public Delivery() {}

    public Delivery(int appId, @NonNull String productName){
        this(appId, LocalDate.now(), productName);
    }

    public Delivery(int id, @NonNull LocalDate createDate, @NonNull String productName){
        this.id = id;
        this.createDate = createDate;
        this.productName = productName;
    }

}
