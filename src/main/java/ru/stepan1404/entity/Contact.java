package ru.stepan1404.entity;

import com.google.common.collect.Sets;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Contact")
@Getter
@Setter
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "contact", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Delivery> deliveries;

    public Contact(int id){
        this(id, Sets.<Delivery>newHashSet());
    }

    public Contact() {}

    public Contact(int id, Set<Delivery> deliveries){
        this.id = id;
        this.deliveries = deliveries;
    }

}
