package ru.stepan1404.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import ru.stepan1404.dto.ContactWidthdrawing;
import ru.stepan1404.entity.Contact;
import ru.stepan1404.exception.NotRegisterContactException;
import ru.stepan1404.repository.ContactRepository;
import ru.stepan1404.service.ContactService;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class ContactServiceImpl implements ContactService {

    @Autowired
    private ContactRepository contactRepository;

    @Override
    public Contact getContact(int id) throws NotRegisterContactException {
        try{
            return contactRepository.findById(id).get();
        } catch (NoSuchElementException e){
            throw new NotRegisterContactException(String.format("Contact with id %d is not registered", id));
        }
    }

    @Override
    public Contact addContact(Contact contact) {
        return contactRepository.saveAndFlush(contact);
    }

    @Override
    public List<ContactWidthdrawing> getContactWithdrawing() {
        return contactRepository.getContactWithdrawing();
    }

    @Override
    public void delete(int id) throws NotRegisterContactException {
        try{
            contactRepository.deleteById(id);
        } catch (IllegalArgumentException e){
            throw new NotRegisterContactException(String.format("Contact with id %d is not registered", id));
        }
    }

    @Override
    public List<Contact> getAll() {
        return contactRepository.findAll();
    }

}
