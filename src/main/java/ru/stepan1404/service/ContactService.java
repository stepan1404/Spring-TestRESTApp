package ru.stepan1404.service;

import ru.stepan1404.dto.ContactWidthdrawing;
import ru.stepan1404.entity.Contact;
import ru.stepan1404.exception.NotRegisterContactException;

import java.util.List;

public interface ContactService {

    Contact getContact(int id) throws NotRegisterContactException;
    Contact addContact(Contact contact);
    List<ContactWidthdrawing> getContactWithdrawing();
    void delete(int id) throws NotRegisterContactException;
    List<Contact> getAll();

}
