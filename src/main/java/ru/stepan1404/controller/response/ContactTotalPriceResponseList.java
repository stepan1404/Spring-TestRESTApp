package ru.stepan1404.controller.response;

import lombok.Getter;
import lombok.Setter;
import ru.stepan1404.dto.ContactWidthdrawing;

import java.util.List;

public class ContactTotalPriceResponseList implements Response {

    @Getter
    @Setter
    private List<ContactWidthdrawing> contactList;

    public ContactTotalPriceResponseList(List<ContactWidthdrawing> contactList){
        this.contactList = contactList;
    }
}
