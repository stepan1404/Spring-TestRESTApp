package ru.stepan1404.controller.response;

import lombok.Getter;
import lombok.Setter;
import ru.stepan1404.entity.Contact;

import java.util.List;

public class ContactListResponse implements Response {

    @Getter
    @Setter
    private List<Contact> contactList;

    public ContactListResponse(List<Contact> contactList){
        this.contactList = contactList;
    }

}
