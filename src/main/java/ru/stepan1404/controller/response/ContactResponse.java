package ru.stepan1404.controller.response;

import lombok.Getter;
import lombok.Setter;
import ru.stepan1404.entity.Contact;

public class ContactResponse implements Response {

    @Getter
    @Setter
    private Contact contact;

    public ContactResponse(Contact contact){
        this.contact = contact;
    }
}
