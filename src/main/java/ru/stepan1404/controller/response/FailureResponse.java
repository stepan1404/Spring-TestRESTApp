package ru.stepan1404.controller.response;

import lombok.Getter;
import lombok.Setter;

public class FailureResponse implements Response {

    @Getter
    @Setter
    private String message;

    public FailureResponse(String message){
        this.message = message;
    }
}
