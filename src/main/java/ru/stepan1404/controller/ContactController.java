package ru.stepan1404.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.stepan1404.controller.response.*;
import ru.stepan1404.exception.NotRegisterContactException;
import ru.stepan1404.service.ContactService;

@RestController
public class ContactController {

    @Autowired
    private ContactService contactService;

    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public Response getAllContact() {
        return new ContactListResponse(contactService.getAll());
    }

    @RequestMapping(value = "/delivery/report", method = RequestMethod.GET)
    public Response getDeliveryReport() {
        return new ContactTotalPriceResponseList(contactService.getContactWithdrawing());
    }

    @RequestMapping(value = "/contact/{id}", method = RequestMethod.GET)
    public Response getContact(@PathVariable(name = "id") Integer id) throws NotRegisterContactException {
        return new ContactResponse(contactService.getContact(id));
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotRegisterContactException.class)
    public @ResponseBody Response handleEntityNotFound(NotRegisterContactException e) {
        return new FailureResponse(e.getMessage());
    }
}
